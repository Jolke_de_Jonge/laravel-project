@extends('layouts.layout')

@section('content')

    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard -> Users -> Show -> ').$user->name }}
            </h2>
        </div>
    </header>

    <div class="max-w-sm rounded overflow-hidden shadow-lg">
        <img class="w-full" src="@isset($user->profile_photo_url) {{ $user->profile_photo_url }} @else 'https://ui-avatars.com/api/?name={{$user->name}}&color=7F9CF5&background=EBF4FF') @endisset "src="/img/card-top.jpg" alt="{{$user->name}}'s Logo}">
        <div class="px-6 py-4">
            <div class="font-bold text-xl mb-2">{{$user->name}}</div>
            <p class="text-gray-700 text-base">
                I have no extra information
            </p>
        </div>
        <div class="px-6 pt-4 pb-2">
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Company: @isset($user->company->name){{$user->company->name}} @else Null @endisset</span>
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Created at: {{$user->created_at}}</span>
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Last login: {{$user->updated_at}}</span>
        </div>
    </div>


@endsection
