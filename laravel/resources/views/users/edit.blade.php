@extends('layouts.layout')



@section('content')

    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard -> Users -> Edit -> ') .$user->name  }}
            </h2>
        </div>
    </header>

    <div class="min-h-screen flex flex-col bg-gray-100">
        <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
            <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
                <h1 class="mb-8 text-3xl text-center">Edit Account</h1>
                <form action="{{ route('users.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input
                        type="text"
                        class="block border border-grey-light w-full p-3 rounded mb-4"
                        name="name" @if(old('name') == '') value="{{$user->name}}" @else value="{{old('name')}}" @endif
                        placeholder="Full Name" />

                    <input
                        type="text"
                        class="block border border-grey-light w-full p-3 rounded mb-4"
                        name="email"
                        placeholder="Email" @if(old('email') == '') value="{{$user->email}}" @else value="{{old('email')}}" @endif />

                    <input
                        type="password"
                        class="block border border-grey-light w-full p-3 rounded mb-4"
                        name="password"
                        placeholder="Password" />
                    <input
                        type="password"
                        class="block border border-grey-light w-full p-3 rounded mb-4"
                        name="confirm_password"
                        placeholder="Confirm Password" />

                    <select name="company" class="block border border-grey-light w-full p-3 rounded mb-4">
                        @isset($user->company_id)

                            <option value="0">No company</option>
                            @else
                            <option value="0" selected>No company</option>
                        @endisset
                        @foreach($companies as $company)
                            @if(old('company') == $company->id)


                                @isset($user->company_id)
                                    @if($user->company_id != $company->id)
                                        <option value="{{$company->id}}" selected>{{$company->name}}</option>
                                    @endif
                                @else
                                    <option value="{{$company->id}}" selected>{{$company->name}}</option>
                                @endisset
                                @else
                                    @isset($user->company_id)
                                        @if($user->company_id != $company->id)
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                        @endif
                                    @else
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endisset
                                @endif

                        @endforeach

                    </select>

                    <button
                        type="submit"
                        class="w-full text-center py-3 rounded bg-green text-white hover:bg-green-dark focus:outline-none my-1"
                    >Update Account</button>

                </form>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>


        </div>
    </div>
@endsection
