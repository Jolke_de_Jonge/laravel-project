@extends('layouts.layout')



@section('content')

    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard -> Company -> Create') }}
            </h2>
        </div>
    </header>

<div class="min-h-screen flex flex-col bg-gray-100">
    <div class="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div class="bg-white px-6 py-8 rounded shadow-md text-black w-full">
            <h1 class="mb-8 text-3xl text-center">Create Company</h1>
            <form action="{{ route('company.store') }}" method="POST">
                @csrf
                <input
                    type="text"
                    class="block border border-grey-light w-full p-3 rounded mb-4"
                    name="name" value="{{old('name')}}"
                    placeholder="Company Name" />

                <button
                    type="submit"
                    class="w-full text-center py-3 rounded bg-green text-white hover:bg-green-dark focus:outline-none my-1"
                >Create Company</button>

            </form>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>


    </div>
</div>
@endsection
