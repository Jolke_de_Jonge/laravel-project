<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="grid grid-cols-1 ">

            @auth
                <a href="{{ route('users.index') }}"
                   class=" mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg text-lg text-gray-700">

                    <div class="p-6">
                        <div class="flex items-center">
                            Users
                        </div>

                    </div>

                </a>
                <a href="{{ route('company.index') }}"
                   class=" mt-2 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg text-lg text-gray-700">

                    <div class="p-6">
                        <div class="flex items-center">
                            Company
                        </div>

                    </div>

                </a>
            @endif

        </div>
    </div>
</x-app-layout>
