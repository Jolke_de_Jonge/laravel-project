<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(20);

        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view ('users.create', ['companies'=> $companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
        ]);
        $request['password'] = bcrypt( $request['password']);
        $request['company'] = (int)$request['company'];


        DB::enableQueryLog(); // Enable query log

        // Your Eloquent query executed by using get()
        $user = new User();
        $user->name = $request['name'];
        $user->password = $request['password'];
        $user->email = $request['email'];
        if($request['company'] != "0") {
            $user->company_id = $request['company'];
        }
        $user->save();

        return redirect()->route('users.index')->with('success', 'User Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $companies = Company::all();
        return view('users.edit', compact('user'), ['companies'=> $companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|max:255|min:4',
            'email' => 'required|email',
        ]);
        $user->name = $request['name'];
        if($request['password'] != "") {
            $request->validate([
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password',
            ]);
            $request['password'] = bcrypt( $request['password']);
            $user->password = $request['password'];
        }
        $user->email = $request['email'];
        if($request['company'] != "0") {
            $request['company'] = (int)$request['company'];
            $user->company_id = $request['company'];
        } else {
            $user->company_id = null;
        }
        $user->update();

        return redirect()->route('users.index')->with('success', 'User Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')
            ->with('success','User is deleted successfully');
    }
}
